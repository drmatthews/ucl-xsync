package org.nrg.xsync.services.remote;

import java.net.InetSocketAddress;
import java.net.Proxy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * @author Mohana Ramaratnam
 *
 */
public abstract class AbstractRemoteRESTService {

		/** The logger. */
		public static final Logger logger = LoggerFactory.getLogger(AbstractRemoteRESTService.class);
		private String proxy_url = "";
		private Integer proxy_port = 3128;
	 
		public String getProxyUrl(){
		   return proxy_url;
		}
	 
		public void setProxyUrl(String proxy_url){
		   this.proxy_url = proxy_url;
		}
	 
		public Integer getProxyPort(){
		   return proxy_port;
		}
	 
		public void setProxyPort(Integer proxy_port){
		   this.proxy_port = proxy_port;
		}
		/**
		 * Gets the resttemplate.
		 *
		 * @return the resttemplate
		 */

		public RestTemplate getResttemplate(){
			
			final SimpleClientHttpRequestFactory requestFactory =new SimpleClientHttpRequestFactory();
			logger.debug(getProxyUrl());
			if (StringUtils.isNotBlank(getProxyUrl())) {
				String proxy_url = getProxyUrl();
				Integer proxy_port = getProxyPort();
				Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxy_url, proxy_port));
				requestFactory.setProxy(proxy);
			}
			requestFactory.setBufferRequestBody(false);
			final RestTemplate template = new RestTemplate(requestFactory); 
			template.setErrorHandler(new XsyncResponseErrorHandler());
			return template;
		}
		
}
