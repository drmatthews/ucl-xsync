package org.nrg.xsync.connection;

import java.util.Date;

/**
 * @author Mohana Ramaratnam
 *
 */

public class RemoteConnection {
	/**
	 * 
	 */
	String url;
	String proxy_url;
	Integer proxy_port;
	String username;
	String password;
	String localProject;
	boolean locked = false;
	Date acquiredTime;
	

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	// include proxy - dan 210220
	public String getProxyUrl() {
		return proxy_url;
	}

	public void setProxyUrl(String proxy_url) {
		this.proxy_url = proxy_url;
	}

	public Integer getProxyPort() {
		return proxy_port;
	}

	public void setProxyPort(Integer proxy_port) {
		this.proxy_port = proxy_port;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getLocalProject() {
		return localProject;
	}

	public void setLocalProject(String localProject) {
		this.localProject = localProject;
	}
	
	public void lock() {
		locked = true;
	}

	public void unlock() {
		locked = false;
	}

	
	public boolean isLocked() {
		return locked;
	}
	
	public Date getAcquiredDate() {
		return acquiredTime;
	}
	
	public void setAcquiredDate() {
		acquiredTime = new Date();
	}
	public void setAcquiredDate(Date d) {
		acquiredTime = d;
	}

}
